from overexpose_histogram import OverExposHistogramGenerator

class UnderExposHistogramGenerator(OverExposHistogramGenerator):

    '''
    
    Generates a dataset of underexposed histograms.

    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

        # If the overexpose range is set to negative, the overexposure flips to underexposure
        self.OVEREXPOSE_RANGE = -.45

if __name__ == "__main__":
    generator = UnderExposHistogramGenerator("../../photogrammetry_datasets/", '../generated_datasets/underexpose_histogramm/')

    generator.start()