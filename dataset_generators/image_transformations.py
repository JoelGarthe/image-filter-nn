import PIL
import numpy as np 
import cv2
import random






def set_image_blurriness(image, random_blur_strength, blur_percent = 1):


    """
    Add blurriness to an image.

    TODO Add more different blur

    Args:
        image : PIL image object
        random_blur_strength : Strength of the applied blur
        blur_percent=1 : Percentage of the image that will be blurred

    """

    # Capping the maximum blur percentage to 100%
    if blur_percent> 1:
        blur_percent = 1

    # Calculate the total square pixels in an image
    total_needed_space = int(image.size[0] * image.size[1])

    # Calculate the square pixels that need to be blurred
    total_needed_space = total_needed_space * blur_percent

    # Randomly generate a fitting mask width
    mask_width = random.randint(int(total_needed_space / image.size[1]), image.size[0])

    # Calculate the matching mask height to match the proper blur image percentage
    mask_height = int(total_needed_space / mask_width)

    # Randomize the blur position in the image
    rand_val = random.random()
    position1 = ( int(rand_val * (image.size[0] - mask_width)), int(rand_val * ( image.size[1] - mask_height )))
    position2 = (  int(position1[0] + mask_width), int(position1[1] + mask_height))

    # Create a mask from the calculated rectangle
    mask = np.zeros((image.size[1], image.size[0], 3), dtype=np.uint8)
    mask = cv2.rectangle(mask, position1, position2, (255,255,255), -1)



    # Blur the image
    image = np.asarray(image)
    blurred_image = PIL.Image.fromarray(np.uint8(cv2.GaussianBlur(image, (( int(10 * random_blur_strength) * 2) + 1, (int( 10 * random_blur_strength) * 2) + 1) , 60,60)))

    # Mask the blurred and normal image with the calculated rectangle mask
    out = np.where(mask==np.array([0, 0, 0]), image, blurred_image)

    
    return out


def set_image_noise(image,prob):
    
    '''
    TODO Rework to resemble actual naturally occuring noise noise

    Add salt and pepper noise to image
    prob: Probability of the noise
    
    '''

    image = np.asarray(image)
    
    output = image.copy()

    if len(image.shape) == 2:
        black = 0
        white = 255            
    else:
        colorspace = image.shape[2]
        if colorspace == 3:  # RGB
            black = np.array([0, 0, 0], dtype='uint8')
            white = np.array([255, 255, 255], dtype='uint8')
        else:  # RGBA
            black = np.array([0, 0, 0, 255], dtype='uint8')
            white = np.array([255, 255, 255, 255], dtype='uint8')
    probs = np.random.random(image.shape[:2])
    output[probs < (prob / 2)] = black
    output[probs > 1 - (prob / 2)] = white
    return PIL.Image.fromarray(np.uint8(output))
