from blur_subimages import BlurSubImageGenerator
import PIL
class BlurGreyGenerator(BlurSubImageGenerator):

    '''
    
    Generates a dataset of grey subimages.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

        
    def save_file(self, data, filename):

        PIL.Image.fromarray(data).convert('L').save(self.target_folder + filename, 'PNG')

    
if __name__ == "__main__":
    generator = BlurGreyGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_sub_grey/')

    generator.start()