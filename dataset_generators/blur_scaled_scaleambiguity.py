from blur_subimages import BlurSubImageGenerator
import numpy as np
import PIL
import image_transformations as imtr
import random



class BlurScaledAmbGenerator(BlurSubImageGenerator):
    """
    
    Generates a dataset where the blur is applied before scaling the whole image down.

    """
    
    def __init__(self, data_folder, target_folder):
        super().__init__(data_folder, target_folder)

        
    def convert(self, image):

        # Changed the order in which the blur is applied
        # Now the blurred is scaled down too

        blur_percentage = round(random.random(), 2)

        img_blurry = imtr.set_image_blurriness(image,  self.defect_amount, blur_percentage)

        img_resized = PIL.Image.fromarray(img_blurry).resize((self.IMG_WIDTH, self.IMG_HEIGHT), PIL.Image.ANTIALIAS)

        filename = str(self.filecounter) + str(blur_percentage) + ".png"
        self.save_file(img_resized, filename)
        
        self.add_data_to_labels(filename, blur_percentage)

        
    def save_file(self, data, filename):

        data.save(self.target_folder + filename, 'PNG')

    

if __name__ == "__main__":
    generator = BlurScaledAmbGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_scaled_scaleambiguity/')

    generator.start()