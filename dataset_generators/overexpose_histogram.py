from dataset_generator import DataGenerator
import numpy as np
import random
import pickle
import PIL
import cv2 as cv


class OverExposHistogramGenerator(DataGenerator):

    '''
    
    Generates a dataset of overexposed histograms.

    '''

    def __init__(self, data_folder, target_folder):
        super().__init__(data_folder, target_folder)

        self.OVEREXPOSE_RANGE = .45


    def extract_features(self, image):

        histogram, bin_edges = np.histogram(image, bins=256, range=(0, 1))
        histogram = (histogram-np.min(histogram))/(np.max(histogram)-np.min(histogram))

        return histogram


    def load(self, filename):
        return PIL.Image.open(filename).convert('L')


    def convert(self, image):

        image = np.array(image) / 255

        # Overexpose image
        overexpose_amount = random.uniform(0, 1)
        img_exp = np.clip((image + (self.OVEREXPOSE_RANGE * overexpose_amount)) , 0, 1) 

        # Get a histogram of the overexposed image
        exp_histogram = self.extract_features(img_exp)
        filename_exp = str(self.filecounter) + "_" + str(overexpose_amount) + "_exp.pkl"

        # Add to the labels and save the data
        self.add_data_to_labels(filename_exp, overexpose_amount)
        self.save_file(exp_histogram, filename_exp)


    def save_file(self, data, filename):

        file_path = self.target_folder + filename

        filehandler = open(file_path ,"wb")
        pickle.dump(data ,filehandler)
        filehandler.close()

    

if __name__ == "__main__":
    generator = OverExposHistogramGenerator("../../photogrammetry_datasets/", '../generated_datasets/overexpose_histogramm/')

    generator.start()