from overexpose_histogram import OverExposHistogramGenerator
import numpy as np
import cv2 as cv

class OverExposHistoRGBGenerator(OverExposHistogramGenerator):

    '''
    
    Generates a dataset of RGB histogram data.
    
    '''

    def load(self, filename):
        return cv.imread(filename)
    

    def __init__(self, data_folder, target_folder):
        super().__init__(data_folder, target_folder)


    def extract_features(self, image):

        # Split the image into the three color channels
        r, g, b = cv.split(image)

        result = np.array([])

        # Create a histogram for each channel seperatly
        for channel in [r,g, b]:
        
            histogram, bin_edges = np.histogram(channel, bins=256, range=(0, 1))

            histogram = (histogram-np.min(histogram))/(np.max(histogram)-np.min(histogram))
            result = np.append(result, histogram)

        return result


if __name__ == "__main__":

    generator = OverExposHistoRGBGenerator("../../photogrammetry_datasets/", '../generated_datasets/overexpose_rgb_histogramm/')

    generator.start()