from overexpose_histogram import OverExposHistogramGenerator
import numpy as np

class OverExposHistoBoxGenerator(OverExposHistogramGenerator):

    '''
    
    Generates a dataset that combines overexposed boxplot and histogram data.
    
    '''


    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

    def extract_features(self, image):

        
        histogram, bin_edges = np.histogram(image, bins=256, range=(0, 1))

        histogram = (histogram-np.min(histogram))/(np.max(histogram)-np.min(histogram))

        median = np.median(image)
        upper_quartile = np.percentile(image, 75)
        lower_quartile = np.percentile(image, 25)

        iqr = upper_quartile - lower_quartile
        upper_whisker = image[image<=upper_quartile+1.5*iqr].max()
        lower_whisker = image[image>=lower_quartile-1.5*iqr].min()
            
        return np.append(histogram, [median, upper_quartile, lower_quartile, upper_whisker, lower_whisker])




if __name__ == "__main__":

    generator = OverExposHistoBoxGenerator("../../photogrammetry_datasets/", '../generated_datasets/overexpose_histobox/')

    generator.start()