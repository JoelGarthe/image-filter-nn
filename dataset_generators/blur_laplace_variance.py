# https://medium.com/snapaddy-tech-blog/mobile-image-blur-detection-with-machine-learning-c0b703eab7de

from blur_subimages import BlurSubImageGenerator
import random
import numpy as np 
from scipy.ndimage import variance
from skimage import io
from skimage.color import rgb2gray
from skimage.filters import laplace
from skimage.transform import resize
import image_transformations as imtr
import PIL

class BlurLaplaceVarianceGenerator(BlurSubImageGenerator):
    """

    TODO Implement
            Current bug where only an empty label is saved.

    Generate laplace variance value and max laplace value and save them. 


    """
    
    

    def __init__(self, data_folder, target_folder):
        super().__init__(data_folder, target_folder)

        self.name = "blur_laplace_variance"
        
    def load(self, filename):

        return io.imread(filename)


    def seperate_img_into_subimages(self, image):

    
        result = []

        img_resized = np.array(resize(image, (self.IMG_WIDTH * 2, self.IMG_HEIGHT * 2)))

        img_resized = rgb2gray(img_resized)

        # Get nearest subcount
        #count = nearest_res_count(img)
        count = 2

        # Cut the image into a few subimages 
        for x in range(count):
            for y in range(count):

                resized_sub = img_resized[self.IMG_WIDTH * x: self.IMG_WIDTH * (x + 1), self.IMG_HEIGHT * y: self.IMG_HEIGHT * (y + 1)]

                # discard everything with a normal dist under 25 to avoid images that are too homogenous

                result.append(resized_sub)       

        return result

    def convert(self, image):

        sub_images = self.seperate_img_into_subimages(image)

        print("s")

        for sub_image in sub_images:


            blur_percentage = round(random.random(), 2)

            edge_laplace = laplace(imtr.set_image_blurriness(PIL.Image.fromarray(sub_image),  self.defect_amount, blur_percentage), ksize=5)

            print([variance(edge_laplace), np.amax(edge_laplace)], blur_percentage)
            self.add_data_to_labels([variance(edge_laplace), np.amax(edge_laplace)], blur_percentage)



if __name__ == "__main__":
    generator = BlurLaplaceVarianceGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_laplace_variance/')

    generator.start()