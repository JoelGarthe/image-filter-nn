from blur_subimages import BlurSubImageGenerator
import random
import numpy as np 
import PIL

class OverexposeImageGenerator(BlurSubImageGenerator):

    '''
    
    Generates a dataset of overexposed 200 x 200 images in RGB format.
    
    '''
    
    def __init__(self, data_folder, target_folder):
        super().__init__(data_folder, target_folder)

        self.IMG_HEIGHT = 200
        self.IMG_WIDTH = 200

        self.OVEREXPOSE_RANGE = .45

    

    def seperate_img_into_subimages(self, image):

        """
        Cut an image into 4 1000 x 1000 subimages and returns them in an array.

        Args:
            image : Input image to cut up.

        """

        result = []
        count = 2

        # Resize the image to cut up the image perfectly
        img_resized = np.array(image.resize((self.IMG_WIDTH * count, self.IMG_HEIGHT * count), PIL.Image.ANTIALIAS))

        # Cut the image into a few subimages 
        for x in range(count):
            for y in range(count):
                resized_sub = img_resized[self.IMG_WIDTH * x: self.IMG_WIDTH * (x + 1), self.IMG_HEIGHT * y: self.IMG_HEIGHT * (y + 1)]
                result.append(resized_sub)       

        return result

    def convert(self, image):

        # Cut up the image into 4 subimages
        sub_images = self.seperate_img_into_subimages(image)

        # Go through all subimages+
        for sub_image in sub_images:

            sub_image = sub_image/255
            
            # Overexpose all image
            overexpose_amount = random.uniform(0, 1)
            filename = str(self.filecounter) + str(overexpose_amount) + ".png"
            img_exp = np.clip((sub_image + (self.OVEREXPOSE_RANGE * overexpose_amount)) , 0, 1) 


            self.save_file(img_exp, filename)            
            self.add_data_to_labels(filename, overexpose_amount)
            self.filecounter +=1
            

    def save_file(self, data, filename):
        PIL.Image.fromarray((data * 255).astype(np.uint8)).save(self.target_folder + filename, 'PNG')


if __name__ == "__main__":
    generator = OverexposeImageGenerator("../../photogrammetry_datasets/", '../generated_datasets/overexpose_subimages/')

    generator.start()