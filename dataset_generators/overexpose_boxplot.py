from overexpose_histogram import OverExposHistogramGenerator
import numpy as np

class OverExposBoxplotGenerator(OverExposHistogramGenerator):

    '''
    
    Generates a dataset of overexposed boxplot data.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

    def extract_features(self, image):

        median = np.median(image)
        upper_quartile = np.percentile(image, 75)
        lower_quartile = np.percentile(image, 25)

        iqr = upper_quartile - lower_quartile
        upper_whisker = image[image<=upper_quartile+1.5*iqr].max()
        lower_whisker = image[image>=lower_quartile-1.5*iqr].min()
            
        return [median, upper_quartile, lower_quartile, upper_whisker, lower_whisker]




if __name__ == "__main__":

    generator = OverExposBoxplotGenerator("../../photogrammetry_datasets/", '../generated_datasets/overexpose_boxplot/')

    generator.start()