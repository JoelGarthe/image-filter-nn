from blur_subimages import BlurSubImageGenerator
import numpy as np
import PIL
import image_transformations as imtr
import random

class BlurScaledGenerator(BlurSubImageGenerator):

    '''
    
    Generates a dataset of images that are scaled down as a whole and subsequently blurred.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)
        

    def convert(self, image):

        img_resized = image.resize((self.IMG_WIDTH, self.IMG_HEIGHT), PIL.Image.ANTIALIAS)

        blur_percentage = round(random.random(), 2)
        filename = str(self.filecounter) + str(blur_percentage) + ".png"
        self.save_file(imtr.set_image_blurriness(img_resized,  self.defect_amount, blur_percentage), filename)
        
        self.add_data_to_labels(filename, blur_percentage)


if __name__ == "__main__":
    generator = BlurScaledGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_scaled/')

    generator.start()