from overexpose_histogram import OverExposHistogramGenerator
import numpy as np
import random

class OverAndUnderexposBoxplotGenerator(OverExposHistogramGenerator):

    '''
    
    Generates a dataset of over and underexposed histograms.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

    def convert(self, image):

        image = np.array(image) / 255

        # Save histogram of clean image
        base_histogram = self.extract_features(image)
        filename = str(self.filecounter) + ".pkl"
        
        # Add to the labels and save the data
        self.add_data_to_labels(filename, 0)
        self.save_file(base_histogram, filename)

        # Overexpose image
        overexpose_amount = random.uniform(-1, 1)
        img_exp = np.clip((image + (self.OVEREXPOSE_RANGE * overexpose_amount)) , 0, 1) 


        exp_histogram = self.extract_features(img_exp)
        filename_exp = str(self.filecounter) + "_" + str(overexpose_amount) + "_exp.pkl"

        # Add to the labels and save the data
        self.add_data_to_labels(filename_exp, overexpose_amount)
        self.save_file(exp_histogram, filename_exp)



if __name__ == "__main__":

    generator = OverAndUnderexposBoxplotGenerator("../../photogrammetry_datasets/", '../generated_datasets/over_and_underexpose_histogram/')

    generator.start()