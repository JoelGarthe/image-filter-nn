from blur_subimages import BlurSubImageGenerator
import numpy as np
import PIL
import image_transformations as imtr
import random
import PIL

class BlurFineGenerator(BlurSubImageGenerator):

    '''
    
    Generate a blur dataset out of subimages with fine blur.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

        self.defect_amount = 0.3


    def save_file(self, data, filename):

        PIL.Image.fromarray(data).save(self.target_folder + filename, 'PNG')

    

if __name__ == "__main__":
    generator = BlurFineGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_rgbe_fine/')

    generator.start()