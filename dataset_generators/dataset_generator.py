import PIL 
from PIL import Image
import glob
import pickle
import numpy as np
from pathlib import Path


class DataGenerator:
    """ 
    Parent Generator interface used to implement different data generators.
    Loads all images in a specific folder, applies a conversion and saves them to a specified folder

    """
    
    def __init__(self, data_folder, target_folder):
        
        """
        Args:
            data_folder : Folder with base images
            target_folder : Folder where the dataset will be generated

        """

        # Lists of label and filenames to save when the conversion is done
        self.labels = []
        self.filenames= []

        # Counting the files to use the counter in filenames
        self.filecounter = 0

        # Fileendings that will be serched for in the source folder
        self.file_endings = ["JPG", "jpg", "png", "PNG"]
        self.data_folder = data_folder

        # Create target folder if it doesnt already exist
        Path(target_folder).mkdir(parents=True, exist_ok=True)
        self.target_folder = target_folder


    
    def write_labels(self):
        """
        Write label data to a labels.pkl file.

        """
    
        label_dump = np.array([self.filenames, self.labels])
    
        filehandler = open(self.target_folder + 'labels.pkl' ,"wb")
        pickle.dump(label_dump, filehandler)
        filehandler.close()
        
        print('>>> Labels written to file.')


    def add_data_to_labels(self, filename, label):
        """
        Add data to the label and filename list

        Args:
            filename : filename to add to the label list
            label : label corresponding to the provided filename

        """
    
        self.filenames.append(filename)
        self.labels.append(label)


    def load(self, filename):
        """
        Loading an image

        Args:
            filename : Path to the file that will be loaded

        """

        return Image.open(filename)
    

    
    def save_file(self, data, filename):
        """
        Saving a file under the specified file name. This method should be overwritten in child classes.

        Args:
            data : file content
            filename : file name

        """
        pass

    
    def convert(self, data):
        """
        Convert the loaded file. This method should be overwritten in child classes.

        Args:
            data : Loaded image

        """
    
        return data


    
    def start(self):

        '''
        Start the conversion process.
        '''

        # Get all subfolders.
        subfolders = glob.glob(self.data_folder + "*")

        print(">>> Found " + str(len(subfolders)) + " folders...")

        for folder in subfolders:
            
            print(">>> Processing " + folder )

            file_list = []

            for ending in self.file_endings:

                # Grab all Files and write them into a list
                file_list.extend(glob.glob(folder + '/*.' + ending))

            for file in file_list:
                self.convert(self.load(file))
                self.filecounter += 1
        
        self.write_labels()

        

        


