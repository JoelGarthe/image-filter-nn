from dataset_generator import DataGenerator
import numpy as np
import PIL
import image_transformations as imtr
import random

class BlurSubImageGenerator(DataGenerator):

    '''
    
    Generates a dataset of blurred subimages. 
    Subimages are 1000 x 1000 patches of an image.
    
    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

        self.IMG_HEIGHT = 1000
        self.IMG_WIDTH = 1000

        # Image Mutation Strength 
        self.defect_amount = 0.5
        
    
    def seperate_img_into_subimages(self, image):
        """
        Cut an image into 4 1000 x 1000 subimages and returns them in an array.

        Args:
            image : Input image to cut up.

        """
    
        result = []
        count = 2

        img_resized = np.array(image.resize((self.IMG_WIDTH * count, self.IMG_HEIGHT * count), PIL.Image.ANTIALIAS))

        # Cut the image into a few subimages 
        for x in range(count):
            for y in range(count):
                resized_sub = img_resized[self.IMG_WIDTH * x: self.IMG_WIDTH * (x + 1), self.IMG_HEIGHT * y: self.IMG_HEIGHT * (y + 1)]

                # Discard everything with a normal dist under 25 to avoid images that are too homogenous
                if np.std(resized_sub) > 25:
                    result.append(resized_sub)       

        return result
    

    def convert(self, image):

        sub_images = self.seperate_img_into_subimages(image)

        for sub_image in sub_images:

            blur_percentage = round(random.random(), 2)
            filename = str(self.filecounter) + str(blur_percentage) + ".png"
            self.save_file(imtr.set_image_blurriness(PIL.Image.fromarray(sub_image),  self.defect_amount, blur_percentage), filename)
            
            self.add_data_to_labels(filename, blur_percentage)

            self.filecounter +=1


    def save_file(self, data, filename):

        PIL.Image.fromarray(data).save(self.target_folder + filename, 'PNG')


if __name__ == "__main__":
    generator = BlurSubImageGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_subimages/')

    generator.start()