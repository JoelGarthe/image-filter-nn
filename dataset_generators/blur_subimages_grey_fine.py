from blur_subimages import BlurSubImageGenerator
import PIL
import PIL
class BlurGreyGenerator(BlurSubImageGenerator):

    '''
    
    Generates a dataset of grey subimages with a fine blur.

    '''

    def __init__(self, data_folder, target_folder):

        super().__init__(data_folder, target_folder)

        self.defect_amount = 0.3
        

    def save_file(self, data, filename):
        PIL.Image.fromarray(data).convert('L').save(self.target_folder + filename, 'PNG')

    
if __name__ == "__main__":
    generator = BlurGreyGenerator("../../photogrammetry_datasets/", '../generated_datasets/blur_grey_fine/')

    generator.start()