from blur_rgbe import BlurDetect
import tensorflow as tf
import imageio
import numpy as np 
import PIL
import cv2 as cv

class BlurDetectGrey(BlurDetect):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.dataset_path = "../generated_datasets/blur_grey_fine/"

        self.epochs = 81

        self.input_shape = (1000,1000,2)

        # Model needs to be defined again to change input (?)
        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=self.input_shape))

        self.model.add(tf.keras.layers.Conv2D(16, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block0_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block0_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block0_Norm'))

        # conv block 1
        self.model.add(tf.keras.layers.Conv2D(32, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block1_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block1_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block1_Norm'))


        # conv block 2
        self.model.add(tf.keras.layers.Conv2D(64, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block2_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block2_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block2_Norm'))

        # conv block 3
        self.model.add(tf.keras.layers.Conv2D(128, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block3_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block3_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block3_Norm'))

        # global max pooling
        self.model.add(tf.keras.layers.GlobalMaxPool2D(name='GlobalMaxPooling'))

        # final layers
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))


    def parse_function(self, filename, label):

        img = np.array(PIL.Image.open(self.dataset_path + filename.numpy().decode('utf-8')).convert('L'))

        edges_blur = cv.Canny(img, 30, 30)

        rgbE = (np.dstack((img, edges_blur)) / 255).reshape(self.input_shape)


        return rgbE, float(label.numpy().decode('utf-8'))




if __name__ == "__main__":
    net = BlurDetectGrey("blur_grey_fine", "../saved_models/")
        
    net.start_training()
