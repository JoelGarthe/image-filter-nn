from blur_rgbe import BlurDetect

class BlurDetectFine(BlurDetect):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.epochs = 82

        self.dataset_path = "../generated_datasets/blur_rgbe_fine/"




if __name__ == "__main__":
    net = BlurDetectFine("blur_rgbe_fine", "../saved_models/")
        
    net.start_training()

