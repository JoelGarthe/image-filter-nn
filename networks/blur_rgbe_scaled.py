from blur_rgbe import BlurDetect

class BlurScaled(BlurDetect):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.dataset_path = "../generated_datasets/blur_scaled/"

        self.epochs = 100




if __name__ == "__main__":
    net = BlurScaled("blur_scaled", "../saved_models/")
        
    net.start_training()
