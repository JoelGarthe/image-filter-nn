from nn import NeuralNet
import tensorflow as tf
import pickle

class OverexposureHistogramNet(NeuralNet):



    def __init__(self, name, save_path):

        super().__init__(name, save_path)

        self.epochs = 300

        self.test_part = 2


        self.dataset_path = "../generated_datasets/overexpose_histogramm/"

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=(256)))

        # Model Structure
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Dense'))
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Dense2'))
        self.model.add(tf.keras.layers.Dropout(.4))
        self.model.add(tf.keras.layers.Dense(512, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))

        self.num_parallel_calls =   4
        self.batch_size =           12
        self.shuffle =              3
        self.num_prefetch =         1


    def parse_function(self, filename, label):

        filename = str(self.dataset_path + filename.numpy().decode('utf-8'))

        file_opener = open(filename,'rb')
        test_image = pickle.load(file_opener)
        file_opener.close()


        label = label.numpy().decode('utf-8')

        return test_image, float(label)


if __name__ == "__main__":
    net = OverexposureHistogramNet("overexpose_histogramm", "../saved_models/")

    net.start_training()

