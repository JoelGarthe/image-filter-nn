from overexpose_histogram import OverexposureHistogramNet
import tensorflow as tf

class OverAndUnderexposureHistogramNet(OverexposureHistogramNet):


    def __init__(self, name, save_path):

        super().__init__(name, save_path)

        self.dataset_path = "../generated_datasets/over_and_underexpose_histogram/"

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=(256)))

        # Model Structure
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Dense'))
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Dense2'))
        self.model.add(tf.keras.layers.Dropout(.4))
        self.model.add(tf.keras.layers.Dense(512, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="tanh", name='output'))


if __name__ == "__main__":
    net = OverAndUnderexposureHistogramNet("over_and_underexpose_histogram", "../saved_models/")

    net.start_training()

