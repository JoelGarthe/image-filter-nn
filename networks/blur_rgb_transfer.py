from nn import NeuralNet
import tensorflow as tf 
import cv2 as cv
import numpy as np
import imageio

class BlurDetectTransfer(NeuralNet):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)


        self.dataset_path = "../generated_datasets/blur_subimages/"

        self.input_shape = (1000, 1000, 3)

        self.epochs = 100

        self.num_parallel_calls =   6
        self.batch_size =           10
        self.shuffle =              3
        self.num_prefetch =         2

        self.base_model = tf.keras.applications.ResNet50(
            input_shape= self.input_shape, 
            include_top=False, 
            weights='imagenet', 
            pooling='avg'
            )
        self.base_model.trainable = False

        # Adding a fully connected layer to interpret the results 
        self.model = tf.keras.Sequential([
            self.base_model,
            tf.keras.layers.Dropout(.4),
            tf.keras.layers.Dense(256, activation='relu', name='hidden_layer'),
            tf.keras.layers.Dense(1, activation='sigmoid', name='output')
        ])


    def parse_function(self, filename, label):

        
        img = imageio.imread(self.dataset_path + filename.numpy().decode('utf-8'))
        rgbE = (img / 255).reshape(self.input_shape)

        return rgbE, float(label.numpy().decode('utf-8'))



if __name__ == "__main__":
    net = BlurDetectTransfer("blur_transfer_ResNet50", "../saved_models/")
        
    net.start_training()

