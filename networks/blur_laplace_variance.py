from nn import NeuralNet

from nn import NeuralNet
import tensorflow as tf 
import cv2 as cv
import numpy as np
import imageio


class BlurLaplaceVariance(NeuralNet):



    def __init__(self, name, save_path):
        super().__init__(name, save_path)


        self.dataset_path = "../generated_datasets/blur_laplace_variance/"

        self.input_shape = (2)

        self.epochs = 100

        self.num_parallel_calls =   4
        self.batch_size =           6
        self.shuffle =              3
        self.num_prefetch =         1



        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=self.input_shape))
        self.model.add(tf.keras.layers.Dense(25, activation="relu", name='Final_Dense1'))
        self.model.add(tf.keras.layers.Dense(50, activation="relu", name='Final_Dense2'))
        self.model.add(tf.keras.layers.Dense(75, activation="relu", name='Final_Dense3'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))


    def parse_function(self, filename, label):


        print(filename)
        print(label)


        return filename, float(label.numpy().decode('utf-8'))



if __name__ == "__main__":
    net = BlurLaplaceVariance("blur_laplace_variance", "../saved_models/")
        
    net.start_training()

