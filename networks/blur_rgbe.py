from nn import NeuralNet
import tensorflow as tf 
import cv2 as cv
import numpy as np
import imageio

class BlurDetect(NeuralNet):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)


        self.dataset_path = "../generated_datasets/blur_subimages/"

        self.input_shape = (1000, 1000, 4)

        self.epochs = 50

        self.test_part = 0

        self.num_parallel_calls =   4
        self.batch_size =           8
        self.shuffle =              3
        self.num_prefetch =         1



        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=self.input_shape))

        self.model.add(tf.keras.layers.Conv2D(16, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block0_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block0_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block0_Norm'))

        # conv block 1
        self.model.add(tf.keras.layers.Conv2D(32, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block1_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block1_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block1_Norm'))


        # conv block 2
        self.model.add(tf.keras.layers.Conv2D(64, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block2_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block2_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block2_Norm'))

        # conv block 3
        self.model.add(tf.keras.layers.Conv2D(128, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block3_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block3_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block3_Norm'))

        # global max pooling
        self.model.add(tf.keras.layers.GlobalMaxPool2D(name='GlobalMaxPooling'))

        self.model.add(tf.keras.layers.Dropout(.4))

        # final layers
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))


    def parse_function(self, filename, label):

        
        img = imageio.imread(self.dataset_path + filename.numpy().decode('utf-8'))

        edges_blur = cv.Canny(img, 30, 30)

        rgbE = (np.dstack((img, edges_blur)) / 255).reshape(self.input_shape)




        return rgbE, float(label.numpy().decode('utf-8'))



if __name__ == "__main__":
    net = BlurDetect("blur_rgbe", "../saved_models/")
        
    net.start_training()

