from overexpose_histogram import OverexposureHistogramNet


class UnderexposureHistogramNet(OverexposureHistogramNet):

    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.epochs = 100

        self.dataset_path = "../generated_datasets/underexpose_histogramm/"

if __name__ == "__main__":
    net = UnderexposureHistogramNet("underexpose_histogramm",  "../saved_models/")

        
    net.start_training()

