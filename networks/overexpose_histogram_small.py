from overexpose_histogram import OverexposureHistogramNet
import tensorflow as tf

class OverexposureHistogramSmall(OverexposureHistogramNet):

    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=(256)))

        # Model Structure
        self.model.add(tf.keras.layers.Dense(128, activation="relu", name='Dense'))
        self.model.add(tf.keras.layers.Dropout(.4))
        self.model.add(tf.keras.layers.Dense(128, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))


        self.dataset_path = "../generated_datasets/overexpose_histogramm/"

if __name__ == "__main__":
    net = OverexposureHistogramSmall("overexpose_histogramm_small",  "../saved_models/")

        
    net.start_training()

