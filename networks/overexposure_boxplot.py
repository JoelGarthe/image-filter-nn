from overexpose_histogram import OverexposureHistogramNet
import tensorflow as tf

class OverexposureBoxplot(OverexposureHistogramNet):

    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=(5)))

        # Model Structure
        self.model.add(tf.keras.layers.Dense(25, activation="relu", name='Dense'))
        
        self.model.add(tf.keras.layers.Dense(50, activation="relu", name='Middle_Dense'))
        self.model.add(tf.keras.layers.Dropout(.4))
        self.model.add(tf.keras.layers.Dense(100, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))


        self.dataset_path = "../generated_datasets/overexpose_boxplot/"

if __name__ == "__main__":
    net = OverexposureBoxplot("overexpose_boxplot",  "../saved_models/")

        
    net.start_training()

