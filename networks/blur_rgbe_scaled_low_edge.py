from blur_rgbe import BlurDetect
import imageio
import cv2 as cv
import numpy as np

class BlurScaled(BlurDetect):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.dataset_path = "../generated_datasets/blur_scaled/"


    def parse_function(self, filename, label):

        
        img = imageio.imread(self.dataset_path + filename.numpy().decode('utf-8'))

        edges_blur = cv.Canny(img, 30, 30)

        rgbE = (np.dstack((img, edges_blur)) / 255).reshape(self.input_shape)




        return rgbE, float(label.numpy().decode('utf-8'))




if __name__ == "__main__":
    net = BlurScaled("blur_scaled", "../saved_models/")
        
    net.start_training()
