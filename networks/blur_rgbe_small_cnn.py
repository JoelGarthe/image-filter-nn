from blur_rgbe import BlurDetect
import tensorflow as tf

class BlurDetectSmallCnn(BlurDetect):
    def __init__(self, name, save_path):
        super().__init__(name, save_path)

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.Input(shape=self.input_shape))

        self.model.add(tf.keras.layers.Conv2D(16, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block0_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block0_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block0_Norm'))

        # conv block 1
        self.model.add(tf.keras.layers.Conv2D(32, (3, 3), activation="relu", kernel_initializer='he_normal', padding='same', name='Block1_Conv'))
        self.model.add(tf.keras.layers.MaxPool2D(pool_size=(2, 2), name='Block1_Pool'))
        self.model.add(tf.keras.layers.BatchNormalization(name='Block1_Norm'))

        # global max pooling
        self.model.add(tf.keras.layers.GlobalMaxPool2D(name='GlobalMaxPooling'))

        # final layers
        self.model.add(tf.keras.layers.Dense(256, activation="relu", name='Final_Dense'))
        self.model.add(tf.keras.layers.Dense(1, activation="sigmoid", name='output'))




if __name__ == "__main__":
    net = BlurDetectSmallCnn("blur_subimages_small_cnn", "../saved_models/")
        
    net.start_training()
