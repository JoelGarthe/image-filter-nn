
import tensorflow as tf 
import os
import numpy as np
import random
import imageio
from keras.callbacks import ModelCheckpoint
import pickle
import math 
import matplotlib.pyplot as plt
from keras.callbacks import CSVLogger
import pandas as pd
from matplotlib.axis import Axis
import os
print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))




class NeuralNet():

    def __init__(self, name, save_path):

        self.seed=2412124

        self.set_global_determinism(self.seed, False)

        self.name = name

        self.dataset_path = ""

        self.epochs = 100
        self.loss_function = 'mean_absolute_error'
        self.metric = 'mean_squared_error'
        self.test_percentage = 0.2
        self.test_part = 3

        self.shuffle_seed = 24124 

        # Training parameters
        self.num_parallel_calls = 4
        self.batch_size = 6
        self.shuffle_size = 3
        self.num_prefetch = 1

        self.optimizer = 'adam'

        self.save_path = save_path + str(self.name)+ "/"
       
        # Checkpoint setting
        self.checkpoint_file_path = self.save_path + self.name + "_checkpoint.hdf5"
        self.checkpoint = ModelCheckpoint(self.checkpoint_file_path , monitor='loss', verbose = 1, save_best_only=True, mode='auto', period=1)
        
        self.history_file = self.save_path + "model_history_log.csv"

        self.model = None

    def start_training(self):

        print(">>> Loading dataset...")

        self.load_dataset()

        print(">>> Model Summary:")
        print(self.model.summary())

        print (">>> Starting training...")

        # Check for a checkpoint and ask the user if he wants to load it
        self.check_for_checkpoint()

        self.model.compile(optimizer=self.optimizer, loss=self.loss_function, metrics=[self.metric])

        csv_logger = CSVLogger(self.history_file, append=True)

        self.history = self.model.fit_generator(self.dataset, epochs = self.epochs, validation_data=self.validation_dataset, callbacks=[self.checkpoint, csv_logger])



        self.save_model()
        


    def save_model(self):

        
        filename = self.name + ".h5"

        if not os.path.exists(self.save_path):
            os.makedirs(self.save_path)

        tf.keras.models.save_model(
            self.model,
            self.save_path + filename,
            overwrite=True,
            include_optimizer=True,
            save_format='h5',
            signatures=None,
            options=None,
            save_traces=True
        )


        self.save_statistics(self.save_path)

    def save_statistics(self, folder_path):

        fig, ax = plt.subplots() 

        columns = ["loss", "val_loss"]
        df = pd.read_csv(self.history_file, usecols=columns)

        ax.plot(df.loss)
        ax.plot(df.val_loss)

        Axis.set_major_locator(ax.xaxis, plt.MaxNLocator(integer=True)) 

        plt.title('loss: ' + self.loss_function)
        plt.ylabel('errors')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig(folder_path + self.loss_function + '.png')


        with open(folder_path + "configuration.txt", "w") as f:

            f.write("Epochs: " + str(self.epochs) + '\n')
            f.write("Loss function: " + self.loss_function + '\n')
            f.write("Metric: " + self.metric + '\n')
            f.write("Seed: " + str(self.seed) + '\n')
            f.write("Test Percentage: " + str(self.test_percentage) + '\n')
            f.write("Test Part: " + str(self.test_part) + '\n')
            f.write("Train data length: " + str(self.train_length) + '\n')
            f.write("Test data length: " + str(self.test_length) + '\n')
            f.write("shuffle_seed: " + str(self.shuffle_seed) + '\n')
            f.write('\n')
            f.write("MODEL SUMMARY" + '\n')
            f.write("===========================" + '\n')

            self.model.summary(print_fn=lambda x: f.write(x + '\n'))

            f.write('\n')


        print(">>> Done :)")



    def check_for_checkpoint(self):
        if os.path.exists(self.checkpoint_file_path ):
            keyboard_input = input(">>> Checkpoint file found. Do you want to load it? (y/n)")

            if keyboard_input == "y":
                self.model.load_weights(self.checkpoint_file_path)

            else:
                if os.path.exists(self.history_file):
                    os.remove(self.history_file)




    def parse_function(self, filename, label):

        #img = np.array(PIL.Image.open(filename.numpy().decode('utf-8')))

        img = imageio.imread(self.dataset_path + filename.numpy().decode('utf-8'))

        return img, float(label)

        

    def load_dataset(self):

        raw_training_data = self.load_raw_data()

        print(">>> Loaded dataset with " + str(len(raw_training_data[0])) + " entries.")

        X_train, Y_train, X_test, Y_test = self.split_dataset(raw_training_data, self.test_percentage, self.test_part) 

        print(X_train)

        self.dataset = tf.data.Dataset.from_tensor_slices((X_train, Y_train))
        self.dataset = self.dataset.map(lambda x, y : tf.py_function(func=self.parse_function, inp=[x, y], Tout=(tf.float32, tf.float32)), num_parallel_calls=self.num_parallel_calls)
        self.dataset = self.dataset.batch(self.batch_size)
        self.dataset = self.dataset.shuffle(self.shuffle_size )
        self.dataset = self.dataset.prefetch(self.num_prefetch)

        self.validation_dataset = tf.data.Dataset.from_tensor_slices((X_test, Y_test))
        self.validation_dataset = self.validation_dataset.map(lambda x, y : tf.py_function(func=self.parse_function, inp=[x, y], Tout=(tf.float32, tf.float32)), num_parallel_calls=self.num_parallel_calls)
        self.validation_dataset = self.validation_dataset.batch(self.batch_size)
        self.validation_dataset = self.validation_dataset.shuffle(self.shuffle_size )
        self.validation_dataset = self.validation_dataset.prefetch(self.num_prefetch)





    def split_dataset(self, data, split_percentage=0.2, part=0):
        
        cut_increment = math.floor(len(data[0]) * split_percentage)

        split_index_low = cut_increment * part
        split_index_high = (cut_increment * part) + cut_increment

        np.random.seed(self.shuffle_seed)
        np.random.shuffle(data[0])

        np.random.seed(self.shuffle_seed)
        np.random.shuffle(data[1])

        X_train= np.concatenate((data[0][:split_index_low], data[0][split_index_high:]))      
        Y_train = np.concatenate((data[1][:split_index_low], data[1][split_index_high:]))


        X_test = data[0][split_index_low : split_index_high]
        Y_test = data[1][split_index_low : split_index_high]

        print(">>> Length training data: ", len(X_train))
        print(">>> Length training labels: ", len(Y_train))
        print(">>> Length test data: ", len(X_test))
        print(">>> Length test labels: ", len(Y_test))

        self.train_length = len(X_train)
        self.test_length = len(X_test)

        return X_train, Y_train, X_test, Y_test


    def load_raw_data(self):
        data = []
        file_opener = open(self.dataset_path + "labels.pkl",'rb')
        data = pickle.load(file_opener)
        file_opener.close()

        return data

    def set_seeds(self, seed):
        os.environ['PYTHONHASHSEED'] = str(seed)
        random.seed(seed)
        tf.random.set_seed(seed)
        np.random.seed(seed)


    def set_global_determinism(self, seed, SEEDING_FLAG):
        
        if SEEDING_FLAG:
        
            self.set_seeds(seed)

            os.environ['TF_DETERMINISTIC_OPS'] = '1'
            os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
            
            tf.config.threading.set_inter_op_parallelism_threads(1)
            tf.config.threading.set_intra_op_parallelism_threads(1)

        else:
            os.environ['TF_DETERMINISTIC_OPS'] = '0'
            os.environ['TF_CUDNN_DETERMINISTIC'] = '0'
