Epochs: 300
Loss function: mean_absolute_error
Metric: mean_squared_error
Seed: 2412124
Test Percentage: 0.2
Test Part: 2
Train data length: 3920
Test data length: 980
shuffle_seed: 24124

MODEL SUMMARY
===========================
Model: "sequential"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 Dense (Dense)               (None, 256)               65792     
                                                                 
 Dense2 (Dense)              (None, 256)               65792     
                                                                 
 dropout (Dropout)           (None, 256)               0         
                                                                 
 Final_Dense (Dense)         (None, 512)               131584    
                                                                 
 output (Dense)              (None, 1)                 513       
                                                                 
=================================================================
Total params: 263,681
Trainable params: 263,681
Non-trainable params: 0
_________________________________________________________________

