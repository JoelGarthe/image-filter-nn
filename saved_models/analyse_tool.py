import pandas as pd
import numpy as np


'''

    Simple script used to calculate the k fold cross validation and other metrics.

'''


model_basename = "./blur_rgbe"

best_scores = []
generalization = []
 
for i in range(0, 5):
    df = pd.read_csv(model_basename + "_part" + str(i) +  "/model_history_log.csv" )
    

    idx = df[['val_loss']].idxmin().values[0]

    best_scores.append(df['val_loss'].min())

    generalization.append(df['val_loss'].min() -df['loss'].min())

print("Generalization Errors: ", generalization)
print("Mean Generalization Error: ", np.mean(generalization))
print("Best scores: ", best_scores)
print("Mean: ", np.mean(best_scores))
print("Standard deviation: ", np.std(best_scores))
print("Prozent Abweichung:", np.std(best_scores) / np.mean(best_scores) * 100)