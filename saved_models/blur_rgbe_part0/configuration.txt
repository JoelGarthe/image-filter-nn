Epochs: 45
Loss function: mean_absolute_error
Metric: mean_squared_error
Seed: 2412124
Test Percentage: 0.2
Test Part: 0
Train data length: 6856
Test data length: 1714
shuffle_seed: 24124

MODEL SUMMARY
===========================
Model: "sequential"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 Block0_Conv (Conv2D)        (None, 1000, 1000, 16)    592       
                                                                 
 Block0_Pool (MaxPooling2D)  (None, 500, 500, 16)      0         
                                                                 
 Block0_Norm (BatchNormaliza  (None, 500, 500, 16)     64        
 tion)                                                           
                                                                 
 Block1_Conv (Conv2D)        (None, 500, 500, 32)      4640      
                                                                 
 Block1_Pool (MaxPooling2D)  (None, 250, 250, 32)      0         
                                                                 
 Block1_Norm (BatchNormaliza  (None, 250, 250, 32)     128       
 tion)                                                           
                                                                 
 Block2_Conv (Conv2D)        (None, 250, 250, 64)      18496     
                                                                 
 Block2_Pool (MaxPooling2D)  (None, 125, 125, 64)      0         
                                                                 
 Block2_Norm (BatchNormaliza  (None, 125, 125, 64)     256       
 tion)                                                           
                                                                 
 Block3_Conv (Conv2D)        (None, 125, 125, 128)     73856     
                                                                 
 Block3_Pool (MaxPooling2D)  (None, 62, 62, 128)       0         
                                                                 
 Block3_Norm (BatchNormaliza  (None, 62, 62, 128)      512       
 tion)                                                           
                                                                 
 GlobalMaxPooling (GlobalMax  (None, 128)              0         
 Pooling2D)                                                      
                                                                 
 dropout (Dropout)           (None, 128)               0         
                                                                 
 Final_Dense (Dense)         (None, 256)               33024     
                                                                 
 output (Dense)              (None, 1)                 257       
                                                                 
=================================================================
Total params: 131,825
Trainable params: 131,345
Non-trainable params: 480
_________________________________________________________________

