Epochs: 30
Input shape: (1120, 1120, 3)
Loss function: binary_crossentropy
Metric: binary_accuracy
Test Percentage: 0.2
Test Part: 2
Train data length: 2292
Test data length: 572

MODEL SUMMARY
===========================
Model: "sequential"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 Final_Dense (Dense)         (None, 256)               65792     
                                                                 
 output (Dense)              (None, 1)                 257       
                                                                 
=================================================================
Total params: 66,049
Trainable params: 66,049
Non-trainable params: 0
_________________________________________________________________
