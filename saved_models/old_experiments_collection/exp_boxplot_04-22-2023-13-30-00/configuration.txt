Epochs: 400
Input shape: 256
Loss function: mean_absolute_error
Metric: mean_squared_error
Test Percentage: 0.2
Test Part: 2
Train data length: 1960
Test data length: 490

MODEL SUMMARY
===========================
Model: "sequential"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 Middle_Layer (Dense)        (None, 25)                150       
                                                                 
 dropout (Dropout)           (None, 25)                0         
                                                                 
 Final_Dense (Dense)         (None, 25)                650       
                                                                 
 output (Dense)              (None, 1)                 26        
                                                                 
=================================================================
Total params: 826
Trainable params: 826
Non-trainable params: 0
_________________________________________________________________
