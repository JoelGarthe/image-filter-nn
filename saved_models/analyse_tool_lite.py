import pandas as pd


'''

    Simple script used to calculate a few metrics from a saved models history.

'''


model_basename = "./overexpose_boxplot"

best_scores = []
generalization = []

df = pd.read_csv(model_basename + "/model_history_log.csv" )

idx = df[['val_loss']].idxmin().values[0]

best_scores.append(df['val_loss'].min())

print("Mean last 5: ", df['val_loss'].tail(5).mean())


print("Min Val Loss: ", df['val_loss'].min())

print("Generalization: ", df['val_loss'].min() -df['loss'].min())



